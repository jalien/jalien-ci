#!/bin/bash
CNT=1
FILES="$(alien_find -l $CNT -o 0 '/alice/data/2018/LHC18b/000285396 pass1/*ESDs.root')"
FILE_COUNT="$(echo $FILES | wc -l)"
[[ "${FILE_COUNT}" -eq "${CNT}" ]] || exit 1
