#!/bin/bash
set -e

if [[ -n "$GITLAB_CI" ]];
then
  echo -e "machine gitlab.cern.ch\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > ~/.netrc
fi

#[[ x"$ALIBUILD_DEBUG" = x"True" ]] && MAYBE_DEBUG="--debug"
MAYBE_DEBUG="--debug"

function setup_ci_cache() {
    if [ -d /persistent ] && [ ! -d /persistent/sw ] && [ -d sw ]; then
        mv sw /persistent/sw
        ls -s /persistent/sw
    fi

    if [ -d /persistent/sw ] && [ -d sw ]; then
        rm -r sw
    fi

    if [ -d /persistent/sw ] && [ ! -L sw ]; then
        ln -s /persistent/sw sw
    fi
}

#ALIDIST_REPO=https://gitlab.cern.ch/nhardi/alidist.git
#ALIDIST_BRANCH=autotools-fix

function setup_repo() {
    src=$1
    dst=$2
    branch=$3

    [[ -n "${FORCE_CLEANUP}" ]] && rm -rf $dst
    [[ -d $dst ]] || git clone $src $dst -b $branch
}

function setup_alidist() {
    setup_repo ${ALIDIST_REPO:-https://github.com/alisw/alidist.git} alidist ${ALIDIST_BRANCH:-master}
    #setup_repo ${ALIDIST_REPO:-https://github.com/yuw726/alidist.git} alidist ${ALIDIST_BRANCH:-add-libuv-to-libwebsockets}
}

function setup_jalien_root() {
    #setup_repo ${JALIEN_ROOT_REPO:-https://gitlab.cern.ch/jalien/jalien-root.git} JAliEn-ROOT ${JALIEN_ROOT_BRANCH:-0.7.5}
    setup_repo ${JALIEN_ROOT_REPO:-https://gitlab.cern.ch/jalien/jalien-root.git} JAliEn-ROOT ${JALIEN_ROOT_BRANCH:-master}
}

function setup_alien_root_legacy() {
    #setup_repo ${ALIEN_ROOT_LEGACY_REPO:-https://gitlab.cern.ch/jalien/alien-root-legacy.git} AliEn-ROOT-Legacy ${ALIEN_ROOT_LEGACY_BRANCH:-0.1.x-patches}
    setup_repo ${ALIEN_ROOT_LEGACY_REPO:-https://gitlab.cern.ch/jalien/alien-root-legacy.git} AliEn-ROOT-Legacy ${ALIEN_ROOT_LEGACY_BRANCH:-master}
}

function setup_jalien() {
    setup_repo ${JALIEN_REPO:-https://gitlab.cern.ch/jalien/jalien.git} JAliEn ${JALIEN_BRANCH:-master}
}

function setup_xjalienfs() {
    setup_repo ${XJALIENFS_REPO:-https://gitlab.cern.ch/jalien/xjalienfs.git} xjalienfs ${XJALIENFS_BRANCH:-master}
}

function setup_jaliendocs() {
    setup_repo ${JALIENDOCS_REPO:-https://gitlab.cern.ch/jalien/jalien-docs.git} jalien-docs ${JALIENDOCS_BRANCH:-master}
}

function setup_agu() {
    #setup_repo ${ALICE_GRID_UTILS_REPO:-https://gitlab.cern.ch/jalien/alice-grid-utils.git} Alice-GRID-Utils ${ALICE_GRID_UTILS_BRANCH:-0.0.6-patches}
    setup_repo ${ALICE_GRID_UTILS_REPO:-https://gitlab.cern.ch/jalien/alice-grid-utils.git} Alice-GRID-Utils ${ALICE_GRID_UTILS_BRANCH:-master}
}

function setup_dependencies() {
    set -x
    setup_alidist
    setup_jalien_root
    #setup_alien_root_legacy
    #setup_xjalienfs
    setup_jalien
    setup_jaliendocs
    #setup_agu
    set +x
}

function build_dependencies() {
    if [ "$TRIGGER_SOURCE" == "JAliEn-ROOT" ]; then
    	aliDoctor JAliEn-ROOT --defaults o2 $MAYBE_DEBUG
    	aliBuild build JAliEn-ROOT --defaults o2 $MAYBE_DEBUG
    	#[ x"DAILY_BUILD" == x"1" ] && aliBuild build AliEn-ROOT-Legacy --defaults prod-latest $MAYBE_DEBUG
        echo "Finished building JAliEn-ROOT"
    else
	    aliBuild build JAliEn --defaults o2 $MAYBE_DEBUG
        echo "Finished building JAliEn"
    fi
    echo $?
}

function build_main() {
    setup_ci_cache
    setup_dependencies
    build_dependencies
    echo $?
}

function usage() {
    echo "build.sh -c -f -k"
}

function parse_args() {
    while getopts "cfk" arg; do
        case "$arg" in
            c)
                echo "Setting the force cleanup option"
                export FORCE_CLEANUP=1
                ;;
            f)
                echo "Doing full tests of all components"
                export FULL_TEST=1
                ;;
            k)
                echo "Setting the keep-going option"
		            export KEEP_GOIND=1
		            ;;
	      esac
    done
}

parse_args $@
build_main
