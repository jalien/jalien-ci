#!/bin/bash
export ROOT="$PWD"
export CVMFS_PREFIX="/cvmfs/alice.cern.ch/bin"
export ALIENPY_DEBUG=1
export ALIENPY_DEBUG_FILE=/tmp/alien_py_log.txt


function setup_jbox_log() {
    if [ -n "{CI}" ]; then
        JBOX_OUTPUT="$ROOT/jbox_output.log"
    else
        JBOX_OUTPUT=/dev/null
    fi
}

function die() {
    echo "Test failed"
    cat /tmp/alien_py_log.txt
    exit 1
}

function setup_ci_credentials() {
    if [ -n "${CI}" ]; then
        cp -r /persistent/.globus $HOME/.globus
        GRID_USER=alienci
#        echo "WARNING: Removing local JAliEn directory!"
#        rm -rf JAliEn # MacOS CI case insesitive exec and local dir in path workaround
    fi
}

function start_jbox() {
    which alienv
    JALIEN_MAIN=alien.JBox alienv setenv VO_ALICE@JAliEn::pro -c bash jalien &>$JBOX_OUTPUT &
    sleep 30 # on MacOS it takes much longer because of some exceptions (portability issues)
}

# NOTE: super dangerous
function stop_jbox() {
    pkill -f JBox
}

function test_jalien_jcentral() {
    local status

    echo "[JCENTRAL]"
    export JALIEN=1
    stop_jbox
    if [ "$TRIGGER_SOURCE" == "JAliEn" ]; then
    	export PATH=/cvmfs/alice.cern.ch/bin:$PATH
    	version=`ls -rt /cvmfs/alice.cern.ch/el7-x86_64/Packages/JAliEn-ROOT  | tail -n1`
    else
	version="latest"
    fi
    which alienv
    echo $version
    #alienv setenv VO_ALICE@JAliEn-ROOT::latest -c bash $ROOT/integration_tests.sh $@
    alienv setenv VO_ALICE@JAliEn-ROOT::$version -c bash $ROOT/integration_tests.sh $@
    status=$?
    unset JALIEN

    return $status
}

function test_jalien_jbox() {
    export PATH=/cvmfs/alice.cern.ch/bin:$PATH
   
    local status
    setup_jbox_log
    echo "JBox output log is: $JBOX_OUTPUT"

    echo "[JBOX]"
    export JBOX=1
    export JALIEN=1
    stop_jbox
    start_jbox

    which alienv
    version=`ls -rt /cvmfs/alice.cern.ch/el7-x86_64/Packages/JAliEn-ROOT  | tail -n1`
    echo $version
    #alienv setenv VO_ALICE@JAliEn-ROOT::latest -c bash $ROOT/integration_tests.sh $@
    alienv setenv VO_ALICE@JAliEn-ROOT::$version -c bash $ROOT/integration_tests.sh $@
    status=$?
    stop_jbox
    unset JBOX
    unset JALIEN

    return $status
}

function test_alien_legacy() {
    local status

    echo "[LEGACY]"
    export LEGACY=1
    alienv setenv VO_ALICE@AliEn-ROOT-Legacy::latest -c bash alien-token-init $GRID_USER
    alienv setenv VO_ALICE@AliEn-ROOT-Legacy::latest -c bash $ROOT/integration_tests.sh $@
    status=$?
    unset LEGACY

    return $status
}

function test_latest_cvmfs() {
    local status

    echo "[CVMFS]"
    export JALIEN=1

    export PATH=/cvmfs/alice.cern.ch/bin:$PATH
    which alienv
    LATEST_ALIPHYSICS=$(alienv q | grep -i 'aliphysics.*root6' | grep -v 20200730 | sort -g | tail -n 1)
    alienv setenv $LATEST_ALIPHYSICS -c bash $ROOT/integration_tests.sh $@
    status=$?

    unset JALIEN

    return $status
}

function test_run_main() {
    test_jalien_jcentral $@ || die
    test_jalien_jbox $@     || die
    test_alien_legacy $@    || die
}

# the entry
setup_ci_credentials

case x"$1" in
    x"")
        test_run_main $@
        ;;
    x"jcentral")
        shift
        test_jalien_jcentral $@ || die
        ;;
    x"jbox")
        shift
        test_jalien_jbox $@ || die
        ;;
    x"legacy")
        shift
        test_alien_legacy $@ || die
        ;;
    x"cvmfs")
        shift
        test_latest_cvmfs $@ || die
        ;;
    *)
        shift
        echo "Something went wrong"
        exit 1
        ;;
esac
